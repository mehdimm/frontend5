describe('Form Validation', () => {
  beforeEach(() => {
    cy.visit('/Contact-form');
  });

  it('should deactivate submit button if form content is invalid', () => {
    // Type invalid input into the form fields
    cy.get('input[name="name"]').type('mehdi');
    cy.get('input[name="email"]').type('invalidemail'); // Insert invalid emai
    // Assert that the submit button is disabled
    cy.get('input[type="submit"]').should('be.disabled');
  });
});

describe('Successful Submission', () => {
  beforeEach(() => {
    cy.visit('/Contact-form'); // Replace '/your-form-page' with the actual URL of your form page
  });
  it('should show correct response from server on successful submission', () => {
    // Type valid input into the form fields
    cy.get('input[name="name"]').type('John Doe');
    cy.get('input[name="email"]').type('john@example.com');
    cy.get('textarea[name="message"]').type('Test message');

    // Submit the form
    cy.get('input[type="submit"]').click();

    cy.on('window:alert', (str) => {
      expect(str).to.equal('Your form has been submitted 201')
    })
  });
});