import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import {storeUser} from '@/utils/Api.js'
import MyForm from '@/components/Form.vue'
import { setActivePinia, createPinia } from 'pinia'

describe('API', () => {
  it('should get response from server', async () => {
    let status = await storeUser("jane doe","jane@doe.com","test");
    expect(status.status).toStrictEqual(201);
  })
})
describe('Form', () => {

  it('should enable submit when  fields are valid', async () => {
    setActivePinia(createPinia());
    const wrapper = mount(MyForm);
    await wrapper.find('input[id="email"]').setValue("pepe@gmail.com");
    await wrapper.find('textarea[id="message"]').setValue("baaaah");
    await wrapper.find('input[id="name"]').setValue("meemaaw");
    const button = await wrapper.find('input[type="submit"]');
    expect(button.attributes('disabled')).toBeUndefined();
  })

  it('should disable submit when any field is invalid', async () => {
    setActivePinia(createPinia());
    const wrapper = mount(MyForm);
    await wrapper.find('input[id="email"]').setValue("pepe@gmail.com");
    await wrapper.find('textarea[id="message"]').setValue(null);
    await wrapper.find('input[id="name"]').setValue(null);
    const button = await wrapper.find('input[type="submit"]');
    expect(button.attributes('disabled')).toBeDefined();
  })

})





