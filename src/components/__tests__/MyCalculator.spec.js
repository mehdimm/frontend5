import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import Calculator from '@/components/Calculator.vue'
describe('Calculator', () => {
  it('should perform basic calculator operations', async () => {
    const wrapper = mount(Calculator);

    await wrapper.find('input').setValue('5+5');
    await wrapper.find('button[id="equal"]').trigger('click');

    expect(wrapper.find('ul#log-list li').text()).toContain('5+5 = 10');
  });

  it('should not allow invalid equations', async () => {
    const wrapper = mount(Calculator);

    await wrapper.find('input').setValue('5/0');
    await wrapper.find('button[id="equal"]').trigger('click');

    expect(wrapper.find('.alert').exists()).toBe(true);
  });

});