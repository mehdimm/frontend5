import { defineStore } from 'pinia'

export const  useTokenStore =  defineStore("tokenStore", {
  state: () => {
    return{
      jwtToken: null,
    }
  },

  actions: {
    saveToken(jwtToken) {
      try{
        this.jwtToken = jwtToken;
      } catch (err){
        console.log(err)
      }
    }
  },
  persist: {
    storage: sessionStorage, // note that data in sessionStorage is cleared when the page session ends
  },

});