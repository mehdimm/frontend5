import { defineStore } from 'pinia'
import { useLocalStorage } from "@vueuse/core"

export const  useFormStore =  defineStore("form", {
  state: () => {
    return{
      email: useLocalStorage('email',null),
      name: useLocalStorage('name',null),
    }
  },

  actions: {
    saveUserInStore(name, email) {
      try{
        this.name = name;
        this.email = email;
      } catch (err){
        console.log(err)
      }
    }
  },
});

