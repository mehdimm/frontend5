import axios from 'axios'

export async function storeUser(name,email,message){
  try{
    return  await axios.post("http://localhost:3000/forms",
      JSON.stringify({ name: name, email: email, message: message }));
  }
  catch (e) {
    console.error('Error:', e);
  }
}
export async function sendExpression(expression, token){
  try{
    return  await axios.post("http://localhost:8080/calculator",
      { expression: expression},
      { headers:
          {
            "Content-Type": "application/json",
            "Authorization" : "Bearer " + token
          }
      }
    );
  }
  catch (e) {
    console.error('Error:', e);
  }
}
export async function retrieveExpressions(pagenumber, token){
  try{
    return  await axios.get("http://localhost:8080/calculator?pageNumber="+pagenumber,
      { headers: { "Authorization" : "Bearer " + token } }
    );
  }
  catch (e) {
    console.error('Error:', e);
  }
}
export async function checkUser(name,password){
  try{
    return  await axios.post("http://localhost:8080/users/verify",
      { userName: name, password: password},
      { headers: { 'Content-Type': 'application/json' } }
    );
  }
  catch (e) {
    console.error('Error:', e);
  }
}
export async function createUser(name,password){
  try{
    return  await axios.post("http://localhost:8080/users/create",
      { userName: name, password: password},
      { headers: { 'Content-Type': 'application/json' } }
    );
  }
  catch (e) {
    console.error('Error:', e);
  }
}